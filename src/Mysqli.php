<?php

/**
 * Partial mysqli wrapper
 */

namespace DjThd2\MysqlPdoCache;

class Mysqli
{
	// TODO
	public $insert_id = '';
	public $affected_rows = '';

	public $field_count = 0;
	public $warning_count = 0;

	public $connect_errno = 0;
	public $connect_error = '';

	public $errno = 0;
	public $error = '';
	public $error_list = [];

	public $sqlstate = '00000';

	public $client_info = '';
	public $client_version = '';
	public $host_info = '';
	public $protocol_version = '';
	public $server_info = '';
	public $server_version = '';

	public $info = '';

	public $thread_id = 0;

	protected $pdo;

	public function __construct(PDO $pdo)
	{
		$this->pdo = $pdo;
	}

	public function __toString()
	{
        return (string)$this->pdo;
    }

    public function autocommit(bool $enable): bool
    {
        // TODO: Not implemented
        throw new \RuntimeException("Mysqli wrapper not compatible with autocommit");
        return false;
    }

    public function begin_transaction(int $flags = 0, string $name = null): bool
    {
        return $this->pdo->beginTransaction();
    }

    public function commit(int $flags = 0, string $name = null): bool
    {
    	return $this->pdo->commit();
    }

    public function connect(...$args): bool
    {
    	return true;
    }

    public function real_connect(...$args): bool
    {
    	return true;
    }

    public function debug(string $options): bool
    {
    	return true;
    }

    public function dump_debug_info(): bool
    {
    	return true;
    }

    public function execute_query(string $query, array $params = null)
    {
    	$stmt = $this->stmt_init();
    	if (!$stmt) {
    		return false;
    	}
    	if (!$stmt->prepare($query)) {
    		return false;
    	}
    	if (!$stmt->execute($params)) {
    		return false;
    	}
    	// TODO: copy properties of stmt into mysqli before returning result
    	return $stmt->get_result();
    }

    public function real_query(string $query): bool
    {
        // TODO: Not implemented
        throw new \RuntimeException("Mysqli wrapper not compatible with real_query");
        return false;
    }

    public function change_user(string $username, string $password, string $database = null): bool
    {
        // TODO: Not implemented
        throw new \RuntimeException("Mysqli wrapper not compatible with change_user");
        return false;
    }

    public function character_set_name(): string
    {
    	return 'utf8mb4';
    }

	public function query(string $query, int $resultmode = \MYSQLI_STORE_RESULT)
	{
		$statement = $this->pdo->query($query);
		if ($statement) {
			$result = new MysqliResult($statement);
		} else {
			$result = false;
		}
		$this->currentStmt = null;
		$this->lastResult = $result;
		return $result;
	}

	public function stmt_init()
	{
		$currentStmt = new MysqliStmt($this, $this->pdo);
		$this->currentStmt = $currentStmt;
		$this->lastResult = null;
		return $currentStmt;
	}

	public function prepare(string $query)
	{
		if ($this->currentStmt) {
			$mysqli_stmt = $this->currentStmt;
		} else {
			$mysqli_stmt = $this->stmt_init();
		}

		if (!$mysqli_stmt) {
			return $mysqli_stmt;
		}

		if ($mysqli_stmt->prepare($query)) {
			return $mysqli_stmt;
		} else {
			$this->currentStmt = null;
			$this->lastResult = null;
			return false;
		}
	}

	public function store_result(int $mode = 0)
	{
		if ($this->lastResult) {
			return $this->lastResult;
		}
		if ($this->currentStmt) {
			return $this->currentStmt->get_result();
		}
		return false;
	}

	public function ping(): bool
	{
		return true;
	}

	public function close()
	{
		return true;
	}

	public function refresh(int $flags): bool
	{
		return true;
	}
	
	public function escape_string($str): string
	{
		return substr($this->pdo->quote((string)$str, \PDO::PARAM_STR), 1, -1);
	}
	
	public function real_escape_string($str): string
	{
		return $this->escape_string($str);
	}

	public function set_charset($charset)
	{
		return true;
	}

	public function get_charset()
	{
        // TODO: Not implemented
        throw new \RuntimeException("Mysqli wrapper not compatible with get_charset");
		return null;
	}

	public function get_warnings()
	{
        // TODO: Not implemented
        throw new \RuntimeException("Mysqli wrapper not compatible with get_warnings");
		return false;
	}

	public function kill(int $process_id): bool
	{
		return false;
	}

	public function more_results(): bool
	{
		return false;
	}

	public function next_result(): bool
	{
		return false;
	}

	public function multi_query(string $query): bool
	{
		throw new \RuntimeException("Mysqli wrapper not compatible with multi_query");
		return false;
	}

	public function release_savepoint(string $name): bool
	{
		throw new \RuntimeException("Mysqli wrapper not compatible with release_savepoint");
		return false;
	}

	public function savepoint(string $name): bool
	{
		throw new \RuntimeException("Mysqli wrapper not compatible with savepoint");
		return false;
	}

	public function rollback(int $flags = 0, string $name = null): bool
	{
		return $this->pdo->rollBack();
	}

	public function options(int $option, $value): bool
	{
		// TODO: implement
		return true;
	}

	public function select_db(string $database): bool
	{
		return true;
	}

	public function ssl_set(...$args): bool
	{
		return true;
	}

	public function stat()
	{
		return 'Uptime: 1  Threads: 1  Questions: 1  Slow queries: 0 Opens: 1  Flush tables: 1  Open tables: 1  Queries per second avg: 1 Memory in use: 1K  Max memory used: 1K';
	}

	public function thread_safe(): bool
	{
		return false;
	}

	public function use_result()
	{
		throw new \RuntimeException("Mysqli wrapper not compatible with use_result");
		return false;
	}

	public function init()
	{
		return true;
	}
}