<?php

/**
 * Partial mysqli_result wrapper
 */

namespace DjThd2\MysqlPdoCache;

class MysqliResult implements \IteratorAggregate
{
    /** @var int */
    public $num_rows = 0;
    public $type = \MYSQLI_STORE_RESULT;
    public $field_count = 0;
    public $current_field = 0;

    /** @var array|false */
    public $lengths = false;

    /** @var PDOStatement */
    protected $statement;

    public function __construct(PDOStatement $statement)
    {
        $this->statement = $statement;
        if ($statement) {
            $this->num_rows = $statement->rowCount();
        }
    }
    
    public function fetch_assoc()
    {
        if (!$this->statement) {
            return false;
        }

        return $this->statement->fetch(\PDO::FETCH_ASSOC);
    }

    public function fetch_array($mode = \MYSQLI_BOTH)
    {
        $result = $this->fetch_assoc();

        if (!$result) {
            return $result;
        }

        if ($mode === \MYSQLI_NUM) {
            $result = array_values($result);
        } else if ($mode === \MYSQLI_BOTH) {
            $result = array_merge($result, array_values($result));
        }

        return $result;
    }

    public function fetch_row()
    {
        return $this->fetch_array(\MYSQLI_NUM);
    }

    public function fetch_column($column = 0)
    {
        $row = $this->fetch_row();

        if (!$row) {
            return false;
        }

        return $row[$column] ?? false;
    }

    public function fetch_object($className = "stdClass", array $constructor_args = [])
    {
        if ($className !== "stdClass") {
            throw new \RuntimeException("MysqliResult wrapper not compatible with fetch_object on custom class");
        }
        $result = $this->fetch_assoc();

        if ($result) {
            return (object)$result;
        } else {
            return false;
        }
    }

    public function fetch_all(int $mode = \MYSQLI_NUM): array
    {
        if (!$this->statement) {
            return [];
        }

        $result = $this->statement->fetchAll(\PDO::FETCH_ASSOC);

        if ($mode === \MYSQLI_NUM) {
            $result = array_map(function($i) {
                return array_values($i);
            }, $result);
        } else if ($mode === \MYSQLI_BOTH) {
            $result = array_map(function($i) {
                return array_merge($i, array_values($i));
            }, $result);
        }

        return $result;
    }

    public function data_seek(int $offset): bool
    {
        // TODO: Not implemented
        throw new \RuntimeException("MysqliResult wrapper not compatible with data_seek");
        return false;
    }

    public function field_seek(int $index): bool
    {
        // TODO: Not implemented
        throw new \RuntimeException("MysqliResult wrapper not compatible with field_seek");
        return false;
    }

    public function fetch_field_direct(int $index)
    {
        // TODO: Not implemented
        throw new \RuntimeException("MysqliResult wrapper not compatible with fetch_field_direct");
        return false;
    }

    public function fetch_field()
    {
        // TODO: Not implemented
        throw new \RuntimeException("MysqliResult wrapper not compatible with fetch_field");
        return false;
    }

    public function fetch_fields(): array
    {
        // TODO: Not implemented
        throw new \RuntimeException("MysqliResult wrapper not compatible with fetch_fields");
        return [];
    }
    
    public function __toString(): string
    {
        return 'Result set';
    }

    public function free()
    {
        $this->statement = null;
        $this->num_rows = 0;
    }

    public function close()
    {
        $this->free();
    }

    public function free_result()
    {
        $this->free();
    }

    public function getIterator(): \Iterator
    {
        // Use generator with ->fetch_row()
        while (($row = $this->fetch_row()) !== false) {
            yield $row;
        }
    }
}