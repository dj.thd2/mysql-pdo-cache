<?php

/**
 * Partial mysqli_stmt wrapper
 */

namespace DjThd2\MysqlPdoCache;

class MysqliStmt
{
	protected $mysqli;
	protected $pdo;
	protected $statement;
	//protected $vars;
	protected $binds;

	protected $result = false;

	// TODO
	public $insert_id = '';
	public $num_rows = '';
	public $affected_rows = '';

	public $param_count = 0;
	public $field_count = 0;
	public $errno = 0;

	public $error = '';
	public $error_list = [];

	public $sqlstate = '00000';

	public $id = 0;
	
	public function __construct(Mysqli $mysqli, PDO $pdo)
	{
		$this->mysqli = $mysqli;
		$this->pdo = $pdo;
		$this->binds = [];
	}

	public function prepare(string $query): bool
	{
		if (!$this->pdo) {
			return false;
		}

		$statement = $this->pdo->prepare($query);
		if (!$statement) {
			return false;
		}

		$this->result = false;

		$this->statement = $statement;
		return true;
	}

	public function attr_get(int $attribute): int
	{
        // TODO: Not implemented
        throw new \RuntimeException("MysqliStmt wrapper not compatible with attr_get");
        return 0;
	}

	public function attr_set(int $attribute, int $value): bool
	{
        // TODO: Not implemented
        throw new \RuntimeException("MysqliStmt wrapper not compatible with attr_set");
        return false;
	}
	
	public function bind_param(string $types, &...$vars): bool
	{
		static $bind_types = [
			'i' => \PDO::PARAM_INT,
			'd' => \PDO::PARAM_STR,
			's' => \PDO::PARAM_STR,
			'b' => \PDO::PARAM_LOB,
		];

		if (!$this->statement) {
			return false;
		}

		$result = true;

		foreach ($vars as $index => &$var) {
			$result &= $this->statement->bindParam($index+1, $var, $bind_types[$types[$index] ?? 's'] ?? \PDO::PARAM_STR);
		}
		
		return $result;
	}
	
	public function execute(array $params = null): bool
	{
		if (!$this->statement) {
			return false;
		}

		$this->result = false;

		$result = $this->statement->execute($params);

		// TODO: Check insert id
		$this->insert_id = $this->pdo->lastInsertId();
		if ($this->insert_id == '') {
			$this->insert_id = null;
		}
		$this->mysqli->insert_id = $this->insert_id;
		$this->num_rows = $this->statement->rowCount();

		return $result;
	}

	public function get_result()
	{
		if (!$this->statement) {
			return false;
		}

		if ($this->result) {
			return $this->result;
		}
		
		$this->result = new MysqliResult($this->statement);
		return $this->result;
	}
	
	public function __toString()
	{
        return 'Prepared statement';
    }
	
	public function bind_result(&...$resultVars)
	{
		$this->binds = [];

		foreach ($resultVars as &$resultVar) {
			$this->binds[] =&$resultVar;
		}
	}

	public function store_result(): bool
	{
		return true;
	}
	
	public function fetch(): bool
	{
		if (!$this->statement) {
			return false;
		}

		$row = $this->statement->fetch(\PDO::FETCH_ASSOC);
		
		if (!$row) {
			return $row;
		}

		$rowValues = array_values($row);

		foreach($rowValues as $i => $value) {
			$this->binds[$i] = $value;
		}

		return true;
	}

	public function free_result()
	{
		$this->result = false;
	}

	public function reset(): bool
	{
		$this->free_result();
		$this->statement = null;
		$this->binds = [];
		$this->insert_id = '';
		$this->num_rows = '';
		$this->affected_rows = '';
		$this->param_count = 0;
		$this->field_count = 0;
		$this->errno = 0;
		$this->error = '';
		$this->error_list = [];
		$this->sqlstate = '00000';
		$this->id = 0;
		return true;
	}
	
	public function close(): bool
	{
		$this->reset();
		$this->mysqli = null;
		$this->pdo = null;
		$this->statement = null;
		$this->binds = [];
		return true;
	}

	public function send_long_data(int $param_num, string $data): bool
	{
        // TODO: Not implemented
        throw new \RuntimeException("MysqliStmt wrapper not compatible with send_long_data");
        return false;
	}

	public function data_seek(int $offset)
	{
        // TODO: Not implemented
        throw new \RuntimeException("MysqliStmt wrapper not compatible with data_seek");
	}

	public function get_warnings()
	{
        // TODO: Not implemented
        throw new \RuntimeException("MysqliStmt wrapper not compatible with get_warnings");
		return false;
	}

	public function result_metadata()
	{
        // TODO: Not implemented
        throw new \RuntimeException("MysqliStmt wrapper not compatible with result_metadata");
		return false;
	}
}