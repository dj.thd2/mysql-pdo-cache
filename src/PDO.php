<?php

/**
 * Query caching PDO wrapper, based on p3-pdo (original header below)
 */

namespace DjThd2\MysqlPdoCache;

/**
 * @package   p3-pdo
 * @see       https://github.com/pine3ree/p3-pdo for the canonical source repository
 * @copyright https://github.com/pine3ree/p3-pdo/blob/master/COPYRIGHT.md
 * @author    pine3ree https://github.com/pine3ree
 * @license   https://github.com/pine3ree/p3-pdo/blob/master/LICENSE.md New BSD License
 */

use function \explode;
use function \func_get_args;

/**
 * CachedPDO is a drop-in replacement for the php-extension "ext-pdo".
 *
 * The purpose of this class is to lazily establish a database connection the
 * first time the connection is needed.
 * 
 * Further customization has been added to act as a transparent query cache handler
 */
class PDO extends \PDO
{
    // For compatibility with PHP 8
    public const FETCH_DEFAULT = 0;

    // Custom attributes
    public const ATTR_PDOCACHE_TTL = 900001;
    public const ATTR_PDOCACHE_UNCACHEABLE_PREFIXES = 900002;
    public const ATTR_PDOCACHE_EXTRA_KEY_DATA = 900003;

    /** @var callable */
    protected $pdoFactory;

    /** @var \PDO|null */
    protected $pdo = null;

    /** @var \Redis|null */
    protected $redis = null;

    /** @var boolean */
    protected $isInTransaction = false;

    /** @var array */
    protected $attributes = [];

    /** @var array */
    protected $customAttributes = [
        self::ATTR_PDOCACHE_TTL => 1800,
        self::ATTR_PDOCACHE_UNCACHEABLE_PREFIXES => [],
        self::ATTR_PDOCACHE_EXTRA_KEY_DATA => '',
    ];

    public function getPdo()
    {
        if ($this->pdo) {
            return $this->pdo;
        }
        $pdo = call_user_func($this->pdoFactory);
        foreach($this->attributes as $attribute => $value) {
            $pdo->setAttribute($attribute, $value);
        }
        $this->pdo = $pdo;
        return $pdo;
    }

    /**
     * {@inheritDoc}
     * 
     * $pdoFactory should be a callable which return a MySQL \PDO instance
     */
    public function __construct(callable $pdoFactory, \Redis $redis, array $customAttributes = [])
    {
        $this->pdoFactory = $pdoFactory;
        $this->redis = $redis;
        foreach ($customAttributes as $key => $value) {
            if (array_key_exists($key, $this->customAttributes)) {
                $this->customAttributes[$key] = $value;
            } else {
                $this->attributes[$key] = $value;
            }
        }
    }

    public function __destruct()
    {
        $this->pdoFactory = null;
        $this->pdo = null;
        $this->redis = null;
        $this->customAttributes = null;
    }

    /** {@inheritDoc} */
    public function beginTransaction(): bool
    {
        $this->isInTransaction = true;
        return $this->getPdo()->beginTransaction();
    }

    /** {@inheritDoc} */
    public function commit(): bool
    {
        $result = $this->getPdo()->commit();
        $this->isInTransaction = false;
        return $result;
    }

    /** {@inheritDoc} */
    public function errorCode(): string
    {
        if ($this->pdo) {
            return $this->pdo->errorCode();
        }

        return '00000';
    }

    /** {@inheritDoc} */
    public function errorInfo(): array
    {
        if ($this->pdo) {
            return $this->pdo->errorInfo();
        }

        return ['00000', null, null];
    }

    /** {@inheritDoc} */
    public function exec($statement): int
    {
        $result = $this->getPdo()->exec($statement);

        $parsedQuery = QueryParser::parse($statement, [
            'uncacheable_table_prefixes' => $this->getAttribute(self::ATTR_PDOCACHE_UNCACHEABLE_PREFIXES),
        ]);
        if ($parsedQuery) {
            $operation = $parsedQuery[QueryParser::OPERATION] ?? null;
            if ($operation === QueryParser::DDL || $operation === QueryParser::DML) {
                $kind = $parsedQuery[QueryParser::KIND] ?? null;

                $relatedObjects = [];
                $tables = [];
                foreach (($parsedQuery[QueryParser::COLUMNS] ?? []) as list($table, $column)) {
                    $tables["t:{$table}"] = true;
                    $relatedObjects[] = "t:{$table}:{$column}";
                }
                $tables = array_keys($tables);
                if ($operation === QueryParser::DML && ($kind === 'INSERT' || $kind === 'DELETE')) {
                    foreach($tables as $tableKey) {
                        $relatedObjects[] = "{$tableKey}:*";
                    }
                }
                if(count($tables) > 0) {
                    array_push($relatedObjects, ...$tables);
                }
                if(count($relatedObjects) > 0) {
                    $pipe = $this->redis->multi(\Redis::PIPELINE);
                    foreach ($relatedObjects as $relatedObject) {
                        $pipe->incr($relatedObject);
                    }
                    $pipe->exec();
                }
            }
        }

        return $result;
    }

    /**
     * {@inheritDoc}
     *
     * If not connected to a database return the attribute value internally stored
     */
    public function getAttribute($attribute)
    {
        if (array_key_exists($attribute, $this->customAttributes)) {
            return $this->customAttributes[$attribute];
        }

        if ($attribute === \PDO::ATTR_DRIVER_NAME) {
            return 'mysql';
        }

        if ($this->pdo) {
            return $this->pdo->getAttribute($attribute);
        }

        switch ($attribute) {
            case \PDO::ATTR_CONNECTION_STATUS:
            case \PDO::ATTR_SERVER_INFO:
            case \PDO::ATTR_SERVER_VERSION:
                return '';
        }

        return array_key_exists($attribute, $this->attributes) ? $this->attributes[$attribute] : null;
    }

    /** {@inheritDoc} */
    public function inTransaction(): bool
    {
        if ($this->pdo) {
            return $this->pdo->inTransaction();
        }

        return $this->isInTransaction;
    }

    /** {@inheritDoc} */
    public function lastInsertId($name = null): string
    {
        if ($this->pdo) {
            return $this->pdo->lastInsertId($name);
        }

        return '';
    }

    /** {@inheritDoc} */
    public function prepare($statement, $driver_options = [])
    {
        $parsed = QueryParser::parse($statement, [
            'uncacheable_table_prefixes' => $this->getAttribute(self::ATTR_PDOCACHE_UNCACHEABLE_PREFIXES),
        ]);

        if ($parsed === null) {
            $parsed = [];
        }

        if ($parsed[QueryParser::NORMALIZED] ?? false) {
            $statement = $parsed[QueryParser::NORMALIZED];
        }

        if ($parsed[QueryParser::UNCACHEABLE] ?? true || $this->isInTransaction) {
            $driver_options[\PDO::ATTR_STATEMENT_CLASS] = [PDOStatement::class, [$this, $this->redis, $parsed]];
            return $this->getPdo()->prepare($statement, $driver_options);
        }

        return new UnboundPreparedPDOStatement($statement, $driver_options, $this, $this->redis, $parsed);
    }

    /**
     * {@inheritDoc}
     * @link https://www.php.net/manual/en/pdo.query.php
     */
    public function query(string $statement, int $fetch_mode = null, $fetch_argument = null, $fetch_extra = null)
    {
        $parsed = QueryParser::parse($statement, [
            'uncacheable_table_prefixes' => $this->getAttribute(self::ATTR_PDOCACHE_UNCACHEABLE_PREFIXES),
        ]);

        if ($parsed === null) {
            $parsed = [];
        }

        if ($parsed[QueryParser::NORMALIZED] ?? false) {
            $statement = $parsed[QueryParser::NORMALIZED];
        }

        $queryParams = func_get_args();
        array_shift($queryParams);

        if ($parsed[QueryParser::UNCACHEABLE] ?? true || $this->isInTransaction) {
            $pdo = $this->getPdo();
            $pdo->setAttribute(\PDO::ATTR_STATEMENT_CLASS, [PDOStatement::class, [$this, $this->redis, $parsed]]);
            $result = $pdo->query($statement, ...array_values($queryParams));

            if ($parsed) {
                $operation = $parsed[QueryParser::OPERATION] ?? null;
                if ($operation === QueryParser::DDL || $operation === QueryParser::DML) {
                    $kind = $parsed[QueryParser::KIND] ?? null;

                    $relatedObjects = [];
                    $tables = [];
                    foreach (($parsed[QueryParser::COLUMNS] ?? []) as list($table, $column)) {
                        $tables["t:{$table}"] = true;
                        $relatedObjects[] = "t:{$table}:{$column}";
                    }
                    $tables = array_keys($tables);
                    if ($operation === QueryParser::DML && ($kind === 'INSERT' || $kind === 'DELETE')) {
                        foreach($tables as $tableKey) {
                            $relatedObjects[] = "{$tableKey}:*";
                        }
                    }
                    if(count($tables) > 0) {
                        array_push($relatedObjects, ...$tables);
                    }
                    if(count($relatedObjects) > 0) {
                        $pipe = $this->redis->multi(\Redis::PIPELINE);
                        foreach ($relatedObjects as $relatedObject) {
                            $pipe->incr($relatedObject);
                        }
                        $pipe->exec();
                    }
                }
            }

            return $result;
        }

        return new UnboundQueryPDOStatement($statement, array_values($queryParams), $this, $this->redis, $parsed);
    }

    /** {@inheritDoc} */
    public function quote($value, $paramtype = null): string
    {
        // If we have a connected PDO then use its native quote method
        if ($this->pdo) {
            return $this->pdo->quote($value, $paramtype);
        }

        // Emulate quote with escapes to avoid establishing a connection
        // TODO: Verify its working properly or at least similar to native quote for each possible case
        switch($paramtype) {
            case \PDO::PARAM_BOOL:
                return (boolean)$value ? 'TRUE' : 'FALSE';
            case \PDO::PARAM_NULL:
                return 'NULL';
            case \PDO::PARAM_INT:
                return (string)intval($value);
            case \PDO::PARAM_LOB:
                return 'X\'' . bin2hex((string)$value) . '\'';
        }
        return '\'' . addcslashes((string)$value, "\x00\n\r\\'\"\x1a") . '\'';
    }

    /** {@inheritDoc} */
    public function rollBack(): bool
    {
        $result = $this->getPdo()->rollBack();
        $this->isInTransaction = false;
        return $result;
    }

    /**
     * {@inheritDoc}
     *
     * Store the attribute internally so that if not connected to a database it
     * may be used when the connection is established
     *
     */
    public function setAttribute($attribute, $value): bool
    {
        if (array_key_exists($attribute, $this->customAttributes)) {
            $this->customAttributes[$attribute] = $value;
            return true;
        }

        $this->attributes[$attribute] = $value;

        if ($this->pdo) {
            return $this->pdo->setAttribute($attribute, $value);
        }

        return true;
    }
}
