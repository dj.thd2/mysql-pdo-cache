<?php

namespace DjThd2\MysqlPdoCache;

class PDOStatement extends \PDOStatement implements \IteratorAggregate {
    protected $pdo;
    protected $redis;
    protected $parsedQuery;

    protected $customAttributes = [
        PDO::ATTR_PDOCACHE_TTL => null,
    ];

    public $cachedQueryString;

    /**
     *
     * $pdo: instance of DjThd2\MysqlPdoCache\PDO
     * 
     */
    protected function __construct(PDO $pdo, \Redis $redis, array $parsedQuery)
    {
        $this->pdo = $pdo;
        $this->redis = $redis;
        $this->parsedQuery = $parsedQuery;
        $this->customAttributes[PDO::ATTR_PDOCACHE_TTL] = $pdo->getAttribute(PDO::ATTR_PDOCACHE_TTL);
    }

    /**
     *
     * Property $queryString is readonly on native \PDOStatement, so we store
     * our own $cachedQueryString to be able to customize it. By default we will
     * populate it with the native $queryString
     * 
     */
    protected function getQueryString()
    {
        if (!$this->cachedQueryString) {
            $this->cachedQueryString = $this->queryString;
        }
        return $this->cachedQueryString;
    }

    protected function fetchYield($mode = PDO::FETCH_DEFAULT)
    {
        while (($row = $this->fetch($mode)) !== false) {
            yield $row;
        }
    }

    public function __destruct()
    {
        $this->pdo = null;
        $this->redis = null;
        $this->parsedQuery = null;
    }

    /**
     *
     * Override execute method to do a selective invalidation of the cache when updating the database
     * 
     */
    public function execute($params = null) {
        $result = parent::execute($params);

        $operation = $this->parsedQuery[QueryParser::OPERATION] ?? null;
        if ($operation === QueryParser::DDL || $operation === QueryParser::DML) {
            $kind = $this->parsedQuery[QueryParser::KIND] ?? null;

            $relatedObjects = [];
            $tables = [];
            foreach (($this->parsedQuery[QueryParser::COLUMNS] ?? []) as list($table, $column)) {
                $tables["t:{$table}"] = true;
                $relatedObjects[] = "t:{$table}:{$column}";
            }
            $tables = array_keys($tables);
            if ($operation === QueryParser::DDL || $kind === 'INSERT' || $kind === 'DELETE') {
                foreach($tables as $tableKey) {
                    $relatedObjects[] = "{$tableKey}:*";
                }
            }
            if(count($tables) > 0) {
                array_push($relatedObjects, ...$tables);
            }
            if(count($relatedObjects) > 0) {
                $pipe = $this->redis->multi(\Redis::PIPELINE);
                foreach ($relatedObjects as $relatedObject) {
                    $pipe->incr($relatedObject);
                }
                $pipe->exec();
            }
        }

        return $result;
    }

    /**
     *
     * Standard PDO Methods (forward to native implementation)
     * 
     */

    public function bindColumn($column, &$variable, $type = \PDO::PARAM_STR, $maxLength = 0, $driverOptions = null) {
        return parent::bindColumn($column, $variable, $type, $maxLength, $driverOptions);
    }

    public function bindParam($param, &$variable, $type = \PDO::PARAM_STR, $maxLength = 0, $driverOptions = null) {
        return parent::bindParam($param, $variable, $type, $maxLength, $driverOptions);
    }

    public function bindValue($param, $value, $type = \PDO::PARAM_STR) {
        return parent::bindValue($param, $value, $type);
    }

    public function closeCursor() {
        return parent::closeCursor();
    }

    public function columnCount() {
        return parent::columnCount();
    }

    public function debugDumpParams() {
        return parent::debugDumpParams();
    }

    public function errorCode() {
        return parent::errorCode();
    }

    public function errorInfo() {
        return parent::errorInfo();
    }

    public function fetch($mode = PDO::FETCH_DEFAULT, $cursorOrientation = \PDO::FETCH_ORI_NEXT, $cursorOffset = 0) {
        return parent::fetch(...func_get_args());
    }

    public function fetchAll($mode = PDO::FETCH_DEFAULT, $param2 = null, $param3 = null) {
        return parent::fetchAll(...func_get_args());
    }

    public function fetchColumn($column = 0) {
        return parent::fetchColumn(...func_get_args());
    }

    public function fetchObject($className = "stdClass", $constructorArgs = []) {
        return parent::fetchObject(...func_get_args());
    }

    public function getAttribute($name) {
        if (array_key_exists($name, $this->customAttributes)) {
            return $this->customAttributes[$name];
        }
        return parent::getAttribute($name);
    }

    public function getColumnMeta($column) {
        return parent::getColumnMeta($column);
    }

    public function getIterator(): \Iterator {
        // PHP 8:
        if (method_exists(parent::class, 'getIterator')) {
            return parent::getIterator();
        }

        // Use generator with ->fetch() for PHP < 8:
        return $this->fetchYield(\PDO::FETCH_ASSOC);
    }

    public function nextRowset() {
        return parent::nextRowset();
    }

    public function rowCount() {
        return parent::rowCount();
    }

    public function setAttribute($attribute, $value) {
        if (array_key_exists($attribute, $this->customAttributes)) {
            $this->customAttributes[$attribute] = $value;
            return true;
        }
        return parent::setAttribute($attribute, $value);
    }

    public function setFetchMode($mode, $param2 = null, $param3 = null) {
        return parent::setFetchMode(...func_get_args());
    }
}