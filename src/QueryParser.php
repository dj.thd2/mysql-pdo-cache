<?php

namespace DjThd2\MysqlPdoCache;

use PHPSQLParser\PHPSQLParser;
use PHPSQLParser\PHPSQLCreator;

class QueryParser
{
    public const ALIASES = 0;
    public const COLUMNS = 1;
    public const NORMALIZED = 2;
    public const KIND = 3;
    public const OPERATION = 4;
    public const PLACEHOLDERS = 5;
    public const UNCACHEABLE = 6;

    public const DDL = 1;
    public const DML = 2;
    public const DCL = 3;
    public const TCL = 4;
    public const DQL = 5;


    // https://stackoverflow.com/questions/7100127/regex-to-match-mysql-comments/13823363
    protected static function trimSqlComments($sql)
    {
        $sqlComments = '@(([\'"`]).*?[^\\\]\2)|((?:\#|--).*?$|/\*(?:[^/*]|/(?!\*)|\*(?!/)|(?R))*\*\/)\s*|(?<=;)\s+@ms';
        /* Commented version
        $sqlComments = '@
            (([\'"`]).*?[^\\\]\2) # $1 : Skip single & double quoted + backticked expressions
            |(                   # $3 : Match comments
                (?:\#|--).*?$    # - Single line comments
                |                # - Multi line (nested) comments
                 /\*             #   . comment open marker
                    (?: [^/*]    #   . non comment-marker characters
                        |/(?!\*) #   . ! not a comment open
                        |\*(?!/) #   . ! not a comment close
                        |(?R)    #   . recursive case
                    )*           #   . repeat eventually
                \*\/             #   . comment close marker
            )\s*                 # Trim after comments
            |(?<=;)\s+           # Trim after semi-colon
            @msx';
        */
        $uncommentedSQL = trim( preg_replace( $sqlComments, '$1', $sql ) );
        // preg_match_all( $sqlComments, $sql, $comments );
        // $extractedComments = array_filter( $comments[ 3 ] );
        // var_dump( $uncommentedSQL, $extractedComments );
        return $uncommentedSQL;
    }

    // https://stackoverflow.com/questions/834303/startswith-and-endswith-functions-in-php
    // Fastest in performance
    protected static function startsWith($haystack, $needle)
    {
        return $haystack[0] === $needle[0]
            ? strncmp($haystack, $needle, strlen($needle)) === 0
            : false;
    }

    protected static function fixExtraSpaces($sql)
    {
        return preg_replace('@(\s*)\.(\s*)@ms', '.', $sql);
    }

    public static $parser;
    public static $creator;

    protected static $parsedQueries = [];

    protected static function parseExpression($item, &$tables, &$colref_pairs, &$placeholders, &$subqueryColumns, &$uncacheable)
    {
        if(!is_array($item) || !isset($item['expr_type'])) {
            return;
        }

        static $uncacheableIdentifiers = [
            'rand' => true,
            'now' => true
        ];

        switch(strtolower($item['expr_type'] ?? '')) {
            case 'colref':
                $parts = $item['no_quotes']['parts'] ?? [$item['base_expr'] ?? null];
                if(!$parts || count($parts) == 0) {
                    $parts = [$item['base_expr'] ?? null];
                }
                $column_name = array_pop($parts);
                $table_name = array_pop($parts);

                if(($column_name[0] ?? '') == ':' || $column_name[0] == '?') {
                    // Is a placeholder
                    $placeholders[] = $column_name;
                    return;
                }

                if($item['alias'] ?? null) {
                    if(is_array($item['alias'])) {
                        $parts = $item['alias']['no_quotes']['parts'] ?? [$item['alias']['name'] ?? null];
                        if(!$parts || count($parts) == 0) {
                            $parts = [$item['alias']['name'] ?? null];
                        }
                        $column_alias = array_pop($parts);
                    } else {
                        $column_alias = $item['alias'];
                    }
                } else {
                    $column_alias = null;
                }

                $colref_pairs[] = [$table_name, $column_alias, $column_name];
                break;
            case 'bracket_expression':
            case 'aggregate_function':
            case 'function':
            case 'expression':
                if($uncacheableIdentifiers[strtolower($item['base_expr'])] ?? null) {
                    $uncacheable = true;
                }
                if($item['sub_tree'] ?? null) {
                    foreach($item['sub_tree'] as $sub_tree) {
                        static::parseExpression($sub_tree, $tables, $colref_pairs, $placeholders, $subqueryColumns, $uncacheable);
                    }
                }
                break;
            case 'table':
                $parts = $item['no_quotes']['parts'] ?? [$item['table'] ?? null];
                if(!$parts || count($parts) == 0) {
                    $parts = [$item['table'] ?? null];
                }
                $table_name = array_pop($parts);

                if($item['alias'] ?? null) {
                    if(is_array($item['alias'])) {
                        $parts = $item['alias']['no_quotes']['parts'] ?? [$item['alias']['name'] ?? null];
                        if(!$parts || count($parts) == 0) {
                            $parts = [$item['alias']['name'] ?? null];
                        }
                        $table_alias = array_pop($parts);
                    } else {
                        $table_alias = $item['alias'];
                    }
                } else {
                    $table_alias = null;
                }

                $tables[] = [$table_alias, $table_name];

                if($item['ref_clause'] ?? null) {
                    foreach($item['ref_clause'] as $ref_clause) {
                        static::parseExpression($ref_clause, $tables, $colref_pairs, $placeholders, $subqueryColumns, $uncacheable);
                    }
                }
                break;
            case 'subquery':
                if($item['ref_clause'] ?? null) {
                    foreach($item['ref_clause'] as $ref_clause) {
                        static::parseExpression($ref_clause, $tables, $colref_pairs, $placeholders, $subqueryColumns, $uncacheable);
                    }
                }
                if($item['sub_tree'] ?? null) {
                    $subquery_results = static::parseDQLQuery($item['sub_tree']);
                    if($subquery_results[static::UNCACHEABLE]) {
                        $uncacheable = true;
                    }
                    foreach($subquery_results[static::COLUMNS] as $subquery_result) {
                        $subqueryColumns[] = $subquery_result;
                    }
                    foreach($subquery_results[static::PLACEHOLDERS] as $placeholder) {
                        $placeholders[] = $placeholder;
                    }
                }
                break;
            case 'column-list':
                if($item['sub_tree'] ?? null) {
                    foreach($item['sub_tree'] as $sub_tree) {
                        static::parseExpression($sub_tree, $tables, $colref_pairs, $placeholders, $subqueryColumns, $uncacheable);
                    }
                }
                break;
        }
    }

    protected static function resolveParsedReferences($tables, $colref_pairs, $placeholders, $subqueryColumns, $uncacheable)
    {
        $table_array = [];
        $table_aliases = [];

        foreach($tables as $table) {
            $table_array[$table[1]] = [
                static::ALIASES => [],
                static::COLUMNS => [],
            ];
            if($table[0]) {
                $table_aliases[$table[0]] = [];
            }
        }

        foreach($tables as $table) {
            if($table[0]) {
                $table_array[$table[1]][static::ALIASES][] = $table[0];
                $table_aliases[$table[0]][] = $table[1];
            }
        }

        $stray_columns = [];

        foreach($colref_pairs as $colref) {
            if($colref[0]) {
                if($table_array[$colref[0]] ?? null) {
                    $table_array[$colref[0]][static::COLUMNS][] = $colref;
                }
                if($table_aliases[$colref[0]] ?? null) {
                    foreach($table_aliases[$colref[0]] as $table_name) {
                        if($table_array[$table_name] ?? null) {
                            $table_array[$table_name][static::COLUMNS][] = $colref;
                        }
                    }
                }
            } else {
                $stray_columns[] = $colref;
            }
        }

        if(count($stray_columns) > 0) {
            $stray_columns = array_map(function($i) { return $i[2]; }, $stray_columns);
        }

        $result = [];

        foreach($subqueryColumns as $column) {
            if(($column[0] ?? null) && ($table_array[$column[0]] ?? null)) {
                $table_array[$column[0]][static::COLUMNS][] = [$column[0], null, $column[1]];
                continue;
            }
            if(!($column[0] ?? null)) {
                $stray_columns[] = $column[1];
                continue;
            }
            $result[] = $column;
        }

        foreach($table_array as $table => $data) {
            $columns = array_map(function($i) { return $i[2]; }, $data[static::COLUMNS]);
            if(count($stray_columns) > 0) {
                $columns = array_merge($columns, $stray_columns);
            }
            $columns = array_unique($columns);
            if(count($columns) == 0) {
                $columns[] = '*';
            }
            if(in_array('*', $columns)) {
                $columns = [[$table, '*']];
            } else {
                $columns = array_map(function($i) use ($table) { return [$table, $i]; }, $columns);
            }
            foreach($columns as $column) {
                $result[] = $column;
            }
        }

        return [
            static::UNCACHEABLE => $uncacheable,
            static::COLUMNS => $result,
            static::PLACEHOLDERS => $placeholders,
        ];
    }

    protected static function parseDQLQuery($parsed)
    {
        $uncacheable = false;
        $subqueryColumns = [];

        $placeholders = [];

        $colref_pairs = [];
        $tables = [];

        static $queryKinds = [
            'SELECT',
            'FROM',
            'WHERE',
            'ORDER',
            'HAVING',
        ];

        foreach($queryKinds as $queryKind) {
            foreach($parsed[$queryKind] ?? [] as $item) {
                static::parseExpression($item, $tables, $colref_pairs, $placeholders, $subqueryColumns, $uncacheable);
            }
        }

        return static::resolveParsedReferences($tables, $colref_pairs, $placeholders, $subqueryColumns, $uncacheable);
    }

    protected static function parseDMLQuery($parsed)
    {
        $uncacheable = true;
        $subqueryColumns = [];

        $colref_pairs = [];
        $tables = [];

        $placeholders = [];

        static $queryKinds = [
            'INSERT',
            'UPDATE',
            'DELETE',
            'REPLACE',
            'FROM',
            'INTO',
            'SET',
        ];

        foreach($queryKinds as $queryKind) {
            foreach($parsed[$queryKind] ?? [] as $item) {
                static::parseExpression($item, $tables, $colref_pairs, $placeholders, $subqueryColumns, $uncacheable);
            }
        }

        return static::resolveParsedReferences($tables, $colref_pairs, $placeholders, $subqueryColumns, $uncacheable);
    }

    protected static function parseDDLQuery($parsed)
    {
        $uncacheable = true;
        $subqueryColumns = [];

        $colref_pairs = [];
        $tables = [];

        $placeholders = [];

        static $queryKinds = [
            'CREATE',
            'ALTER',
            'RENAME',
            'DROP',
            'TRUNCATE',
            'TABLE',
        ];

        foreach($queryKinds as $queryKind) {
            foreach($parsed[$queryKind] ?? [] as $item) {
                static::parseExpression($item, $tables, $colref_pairs, $placeholders, $subqueryColumns, $uncacheable);
            }
        }

        return static::resolveParsedReferences($tables, $colref_pairs, $placeholders, $subqueryColumns, $uncacheable);
    }

    protected static function getQueryKind($parsed)
    {
        static $kinds = ['CREATE', 'ALTER', 'RENAME', 'DROP', 'TRUNCATE', 'INSERT', 'UPDATE', 'DELETE', 'GRANT', 'REVOKE', 'ROLLBACK', 'COMMIT', 'SELECT'];
        foreach($kinds as $kind) {
            if($parsed[$kind] ?? null) {
                return $kind;
            }
        }
        return null;
    }

    protected static function getQueryOperation($queryKind)
    {
        static $operations = [
            // DDL (data definition language)
            'CREATE' => self::DDL,
            'ALTER' => self::DDL,
            'RENAME' => self::DDL,
            'DROP' => self::DDL,
            'TRUNCATE' => self::DDL,
            // DML (data manipulation language)
            'INSERT' => self::DML,
            'UPDATE' => self::DML,
            'DELETE' => self::DML,
            'REPLACE' => self::DML,
            // DCL (data control language)
            'GRANT' => self::DCL,
            'REVOKE' => self::DCL,
            // TCL (transaction control language)
            'ROLLBACK' => self::TCL,
            'COMMIT' => self::TCL,
            // DQL (data query language)
            'SELECT' => self::DQL,
        ];

        return $operations[$queryKind] ?? null;
    }

    public static function parse($originalQuery, $config = [])
    {
        $md5queries = [];

        $md5query = md5($originalQuery, true);
        if (array_key_exists($md5query, static::$parsedQueries)) {
            return static::$parsedQueries[$md5query];
        }
        $md5queries[] = $md5query;

        $query = static::trimSqlComments($originalQuery);
        $query = static::fixExtraSpaces($query);

        $md5query = md5($query, true);
        if (array_key_exists($md5query, static::$parsedQueries)) {
            return static::$parsedQueries[$md5query];
        }
        $md5queries[] = $md5query;

        $parsed = static::$parser->parse($query);
        if(!$parsed) {
            foreach($md5queries as $md5query) {
                static::$parsedQueries[$md5query] = null;
            }
            return null;
        }

        //$message = null;
        $kind = static::getQueryKind($parsed);
        $operation = static::getQueryOperation($kind);

        $result = null;

        switch($operation) {
            case static::DML:
                $result = static::parseDMLQuery($parsed);
                break;
            case static::DQL:
                $result = static::parseDQLQuery($parsed);
                break;
            case static::DDL:
                $result = static::parseDDLQuery($parsed);
                break;
            default:
                //$message = "Not supported kind of query";
                break;
        }

        if ($result) {
            try {
                $normalized = static::$creator->create($parsed);
            } catch(\Throwable $e) {
                $normalized = $originalQuery;
            }
            $result[static::NORMALIZED] = $normalized;
            $result[static::KIND] = $kind;
            $result[static::OPERATION] = $operation;
            $md5queries[] = md5($normalized, true);

            if (!($result[static::UNCACHEABLE] ?? true)) {
                foreach ($result[static::COLUMNS] as list($table, $column)) {
                    if ($config['uncacheable_table_prefixes'] ?? null) {
                        foreach ($config['uncacheable_table_prefixes'] as $prefix) {
                            if (static::startsWith($table, $prefix)) {
                                $result[static::UNCACHEABLE] = true;
                                break;
                            }
                        }
                    }
                }
            }
        }

        foreach ($md5queries as $md5query) {
            static::$parsedQueries[$md5query] = $result;
        }

        return $result;
    }
}

// TODO: Make the class a singleton (maybe) and remove this from here
QueryParser::$parser = new PHPSQLParser();
QueryParser::$creator = new PHPSQLCreator();