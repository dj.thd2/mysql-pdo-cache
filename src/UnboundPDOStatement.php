<?php

namespace DjThd2\MysqlPdoCache;

class UnboundPDOStatement extends PDOStatement implements \IteratorAggregate {
    protected $wrappedStatement = null;

    protected $boundParamVars = [];
    protected $boundParamTypes = [];

    protected $params = null;
    protected $result = false;
    protected $paramsKey = null;
    protected $cursor = 0;

    protected $cachedRelatedObjects = null;

    protected function getWrappedStatement()
    {
        if ($this->wrappedStatement) {
            return $this->wrappedStatement;
        }
        return null;
    }

    protected function executeWrapped()
    {
        if($this->wrappedStatement) {
            return $this->wrappedStatement->execute($this->params);
        } else {
            $statement = $this->getWrappedStatement();
            if ($this->params === null) {
                foreach ($this->boundParamVars as $key => &$variable) {
                    $statement->bindParam($key, $variable, $this->boundParamTypes[$key] ?? \PDO::PARAM_STR, 0, null);
                }
            }
            return $statement->execute($this->params);
        }
    }

    protected function getCachedRelatedObjects(string $cacheKey)
    {
        $data = false;

        $relatedObjects = [];
        $tables = [];

        if ($this->cachedRelatedObjects === null) {
            foreach (($this->parsedQuery[QueryParser::COLUMNS] ?? []) as list($table, $column)) {
                $tables["t:{$table}"] = true;
                $relatedObjects[] = "t:{$table}:{$column}";
            }

            $tables = array_keys($tables);
            if (count($tables) > 0) {
                array_push($relatedObjects, ...$tables);
            }
        }

        $relatedObjects[] = $cacheKey;

        $fetchedData = false;

        do {
            $cachedRelatedObjects = $this->redis->mGet($relatedObjects);

            if (!$fetchedData) {
                $data = array_pop($cachedRelatedObjects);
                array_pop($relatedObjects);
                $fetchedData = true;
            }

            $generateCachedObjects = [];

            foreach ($cachedRelatedObjects as $index => $value) {
                if (!$value) {
                    $value = intval(time() / 600) * 1000;
                    $cachedRelatedObjects[$index] = $value;
                    $generateCachedObjects[$relatedObjects[$index]] = $value;
                }
            }

            if ($generateCachedObjects) {
                $multi = $this->redis->multi(\Redis::PIPELINE);
                foreach ($generateCachedObjects as $key => $value) {
                    $multi->setNx($key, $value);
                }
                $multiResult = $multi->exec();
                $generateCachedObjects = false;
                foreach ($multiResult as $multiResultItem) {
                    if (!$multiResultItem) {
                        $generateCachedObjects = true;
                        break;
                    }
                }
            }
        } while ($generateCachedObjects);

        if ($this->cachedRelatedObjects === null) { 
            $this->cachedRelatedObjects = array_map(function($i) { return intval($i); }, $cachedRelatedObjects);
        }

        return [$data, $this->cachedRelatedObjects];
    }

    protected function getCacheKey($queryString, $params)
    {
        $queryStringKey = $this->pdo->getAttribute(PDO::ATTR_PDOCACHE_EXTRA_KEY_DATA) . '|' . 
            $queryString;
        $md5query = base64_encode(md5($queryStringKey, true));
        if ($params == [[],[],null]) {
            $cacheKey = "q:".$md5query;
        } else {
            $cacheKey = "q:".$md5query.":".base64_encode(md5(json_encode($params), true));
        }
        return $cacheKey;
    }

    protected function isValidData($data, $currentRelatedObjects)
    {
        $storedRelatedObjects = $data[1] ?? [];

        if (count($storedRelatedObjects) != count($currentRelatedObjects)) {
            return false;
        }

        if (count($storedRelatedObjects) == 0) {
            return false;
        }

        foreach ($storedRelatedObjects as $index => $value) {
            if ($value < ($currentRelatedObjects[$index] ?? INF)) {
                return false;
            }
        }

        return true;
    }

    protected function bindParamVar($param, &$variable, $type = \PDO::PARAM_STR)
    {
        $this->params = null;
        $this->result = false;
        $this->paramsKey = null;
        $this->cursor = 0;
        if ($type === \PDO::PARAM_STR) {
            $variable = (string)$variable;
        }
        $this->boundParamVars[$param] = &$variable;
        $this->boundParamTypes[$param] = $type;
    }

    public function bindParam($param, &$variable, $type = \PDO::PARAM_STR, $maxLength = 0, $driverOptions = null)
    {
        $this->bindParamVar($param, $variable, $type);
        if($this->wrappedStatement) {
            return $this->wrappedStatement->bindParam($param, $variable, $type, $maxLength, $driverOptions);
        }
        return true;
    }

    public function bindValue($param, $value, $type = \PDO::PARAM_STR)
    {
        $this->bindParamVar($param, $value, $type);
        if($this->wrappedStatement) {
            return $this->wrappedStatement->bindValue($param, $value, $type);
        }
        return true;
    }

    public function execute($params = null)
    {
        $this->cursor = 0;
        $this->params = $params;
        $this->paramsKey = [$this->boundParamVars, $this->boundParamTypes, $params];
        $this->result = false;

        if (($this->parsedQuery[QueryParser::UNCACHEABLE] ?? true) || $this->pdo->inTransaction()) {
            return $this->executeWrapped();
        }

        $cacheKey = $this->getCacheKey($this->getQueryString(), $this->paramsKey);
        $result = $this->getCachedRelatedObjects($cacheKey);

        list($data, $cachedRelatedObjects) = $result;

        if ($this->isValidData($data, $cachedRelatedObjects)) {
            $this->result = $data;
            return true;
        }

        return $this->executeWrapped();
    }

    public function fetchAll($mode = PDO::FETCH_DEFAULT, $param2 = null, $param3 = null)
    {
        if ($mode === PDO::FETCH_DEFAULT) {
            $mode = \PDO::FETCH_ASSOC;
        }

        if ($mode !== \PDO::FETCH_ASSOC) {
            // Only supported mode is \PDO::FETCH_ASSOC (2)
            if (!$this->wrappedStatement) {
                $this->executeWrapped();
            }
            return $this->wrappedStatement->fetchAll(...func_get_args());
        }

        if ($this->result !== false) {
            return $this->result[0] ?? false;
        }

        if ($this->paramsKey === null || $this->params === null) {
            $this->paramsKey = [$this->boundParamVars, $this->boundParamTypes, $this->params];
        }

        $cacheKey = $this->getCacheKey($this->getQueryString(), $this->paramsKey);
        $result = $this->getCachedRelatedObjects($cacheKey);

        list($data, $cachedRelatedObjects) = $result;

        if ($this->isValidData($data, $cachedRelatedObjects)) {
            $this->result = $data;
            return $data[0] ?? false;
        }

        if (!$this->wrappedStatement) {
            $this->executeWrapped();
        }

        $result = $this->wrappedStatement->fetchAll(...func_get_args());
        if ($result !== false) {
            $result = [$result, $cachedRelatedObjects];
            $this->redis->setEx($cacheKey, $this->customAttributes[PDO::ATTR_PDOCACHE_TTL], $result);
        }

        $this->result = $result;
        return $this->result[0] ?? false;
    }

    public function fetch($mode = PDO::FETCH_DEFAULT, $cursorOrientation = \PDO::FETCH_ORI_NEXT, $cursorOffset = 0)
    {
        if ($mode === PDO::FETCH_DEFAULT) {
            $mode = \PDO::FETCH_ASSOC;
        }

        // Only supporting the default arguments for now
        if ($mode !== \PDO::FETCH_ASSOC || $cursorOrientation !== \PDO::FETCH_ORI_NEXT || $cursorOffset !== 0) {
            // Only supported mode is \PDO::FETCH_ASSOC (2)
            if (!$this->wrappedStatement) {
                $this->executeWrapped();
            }
            return $this->wrappedStatement->fetch(...func_get_args());
        }

        if ($this->result !== false) {
            return $this->result[0][$this->cursor++] ?? false;
        }

        $result = $this->fetchAll($mode);

        if ($this->result !== false) {
            return $this->result[0][$this->cursor++] ?? false;
        } else {
            $this->result = [$result, []];
            return $result[$this->cursor++] ?? false;
        }
    }

    public function getIterator(): \Iterator
    {
        $result = $this->fetchAll(\PDO::FETCH_ASSOC);

        if ($this->result === false) {
            $this->result = [$result, []];
        }

        if (!$result) {
            return new \ArrayIterator([]);
        }

        return new \ArrayIterator($result);
    }

    public function rowCount()
    {
        $result = $this->fetchAll(\PDO::FETCH_ASSOC);

        if (!$result) {
            return 0;
        }

        return count($result);
    }
}