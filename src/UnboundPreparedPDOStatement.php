<?php

namespace DjThd2\MysqlPdoCache;

class UnboundPreparedPDOStatement extends UnboundPDOStatement {
    protected $driverOptions;

    public function __construct(string $queryString, array $driverOptions, PDO $pdo, \Redis $redis, array $parsedQuery)
    {
        parent::__construct($pdo, $redis, $parsedQuery);
        $this->cachedQueryString = $queryString;
        $this->driverOptions = $driverOptions;
    }

    protected function getWrappedStatement()
    {
        if ($this->wrappedStatement) {
            return $this->wrappedStatement;
        }
        if ($this->pdo->getPdo()) {
            $driverOptions = $this->driverOptions;
            $driverOptions[\PDO::ATTR_STATEMENT_CLASS] = [PDOStatement::class, [$this->pdo, $this->redis, $this->parsedQuery]];
            $this->wrappedStatement = $this->pdo->getPdo()->prepare($this->getQueryString(), $driverOptions);
        }
        return $this->wrappedStatement;
    }
}