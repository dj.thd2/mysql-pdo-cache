<?php

namespace DjThd2\MysqlPdoCache;

class UnboundQueryPDOStatement extends UnboundPDOStatement {
    protected $queryParams = [];

    public function __construct(string $queryString, array $queryParams, PDO $pdo, \Redis $redis, array $parsedQuery)
    {
        parent::__construct($pdo, $redis, $parsedQuery);
        $this->cachedQueryString = $queryString;
        $this->queryParams = $queryParams;
    }

    protected function getWrappedStatement()
    {
        if ($this->wrappedStatement) {
            return $this->wrappedStatement;
        }
        if ($this->pdo->getPdo()) {
            $pdo = $this->pdo->getPdo();
            $pdo->setAttribute(\PDO::ATTR_STATEMENT_CLASS, [PDOStatement::class, [$this->pdo, $this->redis, $this->parsedQuery]]);
            $this->wrappedStatement = $pdo->query($this->getQueryString(), ...$this->queryParams);
        }
        return $this->wrappedStatement;
    }
}